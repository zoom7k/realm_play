import UIKit
import RealmSwift

/*
    https://realm.io/docs/swift/latest#default-realm
*/

class ViewController: UIViewController {

    let delete: Bool =                      true
    let random: Bool =                      true
    let writes: Bool =                      true
        
    var account_realm_remote:Realm?
    var account_realm_local:Realm?
    var account_realm_current:Realm?
    
    var accounts:                           Results<Account>?
    var token:                              NotificationToken?
    
    
    @IBOutlet weak var addButton:           UIButton!
    @IBOutlet weak var updateAuthButton:    UIButton!
    @IBOutlet weak var deleteAccountButton: UIButton!
    @IBOutlet weak var observeButton:       UIButton!
    @IBOutlet weak var consoleView:         UITextView!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        /*
            Get started a synchronous realm using the same config as per async
         */
        let config_accounts = SyncUser.current?.configuration(realmURL: Constants.REALM_URL_ACCOUNTS, fullSynchronization: true)
        account_realm_local = try! Realm(configuration: config_accounts!)
        account_realm_current = account_realm_local
        
        active(value: false)
        /*
           Login and Get asynchronous realm and replace the synchronous realm
        */
        logIn(username: "naresh.maharaj", password: "password", register: false)
    }
    
    
    func active(value: Bool) {
        addButton.isEnabled = value
        updateAuthButton.isEnabled = value
        deleteAccountButton.isEnabled = value
        observeButton.isEnabled = value
    }
       
    
    // MARK ACTIONS
    @IBAction func add(_ sender: UIButton) {
        // Create a new Realm Account object
        let account = createAccount()
        
        // Add Account to the Realm Database
        if writes {
            try! account_realm_current!.write {
                   account_realm_current!.add(account)
            }
        }
    }
    
    
  
    @IBAction func observerAccounts(_ sender: UIButton) {
        /*
            Get me all changes in Accounts that are not authorised
         */
//        let results = account_realm?.objects(Account.self).filter("authorised=true")
        let results = account_realm_current?.objects(Account.self)

        token = results?.observe() { changes in

            switch changes {
            case .initial: break
                // print( results )
            case .update(_, let deletions, let insertions, let modifications): break
            case .error(let error): break
            }
//            print( changes )
            self.consoleView.text = results?.description
        }
    }
    
    @IBAction func deleteAccounts(_ sender: UIButton) {
         if delete { deleteAllAccounts() }
    }
    
    
    @IBAction func updateAuth(_ sender: UIButton) {
        let all_accounts = account_realm_current?.objects(Account.self)
               try! account_realm_current!.write {
                all_accounts?.setValue(true, forKeyPath: "authorised")
               }
    }
    
    //MARK HELPERS
    func deleteAllAccounts() {
        if !account_realm_current!.isEmpty {
            try! account_realm_current!.write {
                account_realm_current!.deleteAll()
            }
        }
    }
    
    
    func createAccount() -> Account
    {
        let accountNumber = random ? "0" + String(Int.random(in: 1...99999999)) : "31673629"
        let telephoneNumber = random ? "07773" + String(Int.random(in: 100000...999999)) : "07825810090"
                
        // Create a new Realm Account object
        let account = Account.accountBuilder()
            .withAccountId(accountId: accountNumber)
            .withPhoneNumber(phoneNumber: telephoneNumber)
            .build()
        
        return account
    }
    
    
    
    // Log in with the username and password, optionally registering a user.
    func logIn(username: String, password: String, register: Bool) {
        
        print("Log in as user: \(username) with register: \(register)");
        
        let creds = SyncCredentials.usernamePassword(username: username, password: password, register: register);
        
        SyncUser.logIn(with: creds, server: Constants.AUTH_URL, onCompletion: { (user, err) in
            if let error = err
            {
                print("Login failed: \(error.localizedDescription)");
                return
            }
            print("Login succeeded!");
            
            // Enable Add Account Button
            self.active(value: true)
            
            /*
                Set up the Account Realm
                https://docs.realm.io/sync/using-synced-realms/setting-up-your-realms#asynchronously-opening-a-realm
             */
            let config_accounts = SyncUser.current?.configuration(realmURL: Constants.REALM_URL_ACCOUNTS, fullSynchronization: true)
            Realm.asyncOpen(configuration: config_accounts!) { realm, error in
                if let realm = realm
                {
                    // Realm successfully opened, with all remote data available
                    self.account_realm_remote = realm
                    self.account_realm_current = self.account_realm_remote
                   
                    // Get all the accounts
                    self.accounts = self.account_realm_current?.objects(Account.self)
                }
                else if let error = error
                {
                    // Handle error that occurred while opening or downloading the contents of the Realm
                    print( error )
                }
            }
        })
    }
}
