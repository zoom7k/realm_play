//
//  Account.swift
//  realm_example
//
//  Created by Naresh Maharaj on 23/10/2019.
//  Copyright © 2019 Naresh Maharaj. All rights reserved.
//

/*
    https://github.com/realm/realm-cocoa/issues/5973
 
    After spending almost two days on this issue, I might have found the solution on my own.
    Xcode sometimes seems to not be able to compile RealmSwift
    source (error: "Could not build Objective-C module 'RealmSwift'").
    Please incorporate my solution to the problem in your docs.

    To overcome this issue, the module needs to be built on the project:

    Go To: Product > Schemes > New Scheme...
    Select: RealmSwift and click OK
    Build the RealmSwift target (cmd + b)
 
    After this you will need to switch the scheme back the Project
 */

import Foundation
import RealmSwift

class Account : Object
{    
    @objc dynamic var accountId : String = ""
    @objc dynamic var phoneNumber : String = ""
    @objc dynamic var authorised : Bool = false
    @objc dynamic var smsAuthenticationCode : String = ""
    @objc dynamic var deviceToken : String = ""
    @objc dynamic var publicKey : String = ""
    
    
    override static func primaryKey() -> String? {
        return "phoneNumber"
    }
    
    
    override public var hash: Int {
        return phoneNumber.hashValue
    }


    public static func == (lhs: Account, rhs: Account) -> Bool {
        return equality(lhs: lhs, rhs: rhs)
    }

    /*
     Required for checking existence in Sets and Lists
     */
    override public func isEqual(_ object: Any?) -> Bool {
        guard let rhs = object as? Account else {
            return false
        }
        let lhs = self

        return Account.equality(lhs: lhs, rhs: rhs)
    }

    public static func equality (lhs: Account, rhs: Account) -> Bool {
        return lhs.accountId == rhs.accountId &&
            lhs.phoneNumber == rhs.phoneNumber &&
            lhs.authorised == rhs.authorised &&
            lhs.smsAuthenticationCode == rhs.smsAuthenticationCode &&
            lhs.deviceToken == rhs.deviceToken &&
            lhs.publicKey == rhs.publicKey
    }

    func getAccountId() ->  String {
        return self.accountId
    }

    func getPhoneNumber() -> String {
        return self.phoneNumber
    }

    func isAuthorised() -> Bool {
        return self.authorised
    }

    func getDeviceToken() -> String {
        return self.deviceToken
    }

    func getPublicKey() -> String {
        return self.publicKey
    }

    func getSMSAuthenticationCode() -> String {
        return self.smsAuthenticationCode
    }

    func toDictionary() -> Dictionary<String,String> {
           return [
               "account_id": accountId ,
               "phone_number" : phoneNumber,
               "account_authorised" : authorised ? "True" : "False",
               "sms_authentication_code" : smsAuthenticationCode,
               "device_token" : deviceToken,
               "public_key" : publicKey
           ]
    }

    class func updateDeviceToken(account: Account, deviceToken: String) ->  Account {
        return accountBuilder()
            .withDeviceToken(deviceToken: deviceToken, account: account)
            .build();
    }

    class func authorise(account: Account, smsAuthenticationCode: String) -> Account {
        return accountBuilder()
            .withSMSAuthenticationCode(smsAuthenticationCode: smsAuthenticationCode, account: account)
            .build()
    }

    class func emptyAccount() -> Account {
        return Account()
    }

    class func accountBuilder() -> AccountBuilder {
        return AccountBuilder()
    }

    class AccountBuilder {
        private var accountId : String = "0"
        private var phoneNumber : String = ""
        private var authorised : Bool = false
        private var smsAuthenticationCode : String = ""
        private var deviceToken : String = ""
        private var publicKey : String = ""

        func withAccountId(accountId: String ) -> AccountBuilder {
            self.accountId = accountId;
            return self
        }

        func withPhoneNumber(phoneNumber: String ) -> AccountBuilder {
            self.phoneNumber = phoneNumber;
            return self
        }

        func withAuthorised(auth: Bool ,account: Account) -> AccountBuilder {
            withAccount(account: account)
            self.authorised = auth
            return self
        }
        func withSMSAuthenticationCode(smsAuthenticationCode: String, account: Account) -> AccountBuilder {
            withAccount(account: account)
            self.smsAuthenticationCode = smsAuthenticationCode
            self.deviceToken = account.deviceToken
            return self
        }

        func withDeviceToken(deviceToken: String, account: Account) -> AccountBuilder {
            withAccount(account: account)
            self.smsAuthenticationCode = account.smsAuthenticationCode
            self.deviceToken = deviceToken
            return self
        }

        func build() -> Account {
            let account = Account()
            account.accountId = self.accountId
            account.phoneNumber = self.phoneNumber
            account.authorised = self.authorised
            account.smsAuthenticationCode = self.smsAuthenticationCode
            account.deviceToken = self.deviceToken
            return account
        }

        private func withAccount(account: Account){
            self.accountId = account.accountId
            self.phoneNumber = account.phoneNumber
            self.publicKey = account.publicKey
        }
    }
    
    public class func accountArray(account: Account) -> Array<Account> {
        var arr = Array<Account>()
        arr.append(account)
        return arr
    }
    
}
