//
//  RealmPlayTests.swift
//  RealmPlayTests
//
//  Created by Naresh Maharaj on 23/10/2019.
//  Copyright © 2019 Naresh Maharaj. All rights reserved.
//

import XCTest
@testable import RealmPlay

class RealmPlayTests: XCTestCase {

   let expectedPhoneNumber = "077718561234"

   override func setUp() {
       super.setUp()
       // Put setup code here. This method is called before the invocation of each test method in the class.
   }

   override func tearDown() {
       super.tearDown()
       // Put teardown code here. This method is called after the invocation of each test method in the class.
   }

   func testShouldBuildAccount() {
          // This is an example of a functional test case.
          // Use XCTAssert and related functions to verify your tests produce the correct results.
          assertAccount(account: RealmPlayTests.accountFor(phoneNumber: expectedPhoneNumber))
   }
    
    func testShouldEqual()
    {
        XCTAssertTrue( RealmPlayTests.accountFor(phoneNumber: expectedPhoneNumber) == RealmPlayTests.accountFor(phoneNumber: expectedPhoneNumber))
        
        var array1 = [Account]()
        var array2 = [Account]()
        
        array1.append(RealmPlayTests.accountFor(phoneNumber: expectedPhoneNumber))
        array2.append(RealmPlayTests.accountFor(phoneNumber: expectedPhoneNumber))
        
        XCTAssertTrue( array1 == array2 )                
    }

    func testShouldIsEqual()
    {
        var array1 = [Account]()
        array1.append(RealmPlayTests.accountFor(phoneNumber: expectedPhoneNumber))
        
        XCTAssertTrue( array1.contains( RealmPlayTests.accountFor(phoneNumber: expectedPhoneNumber) ) )
    }
    
   class func accountFor(phoneNumber: String) -> Account {
       return Account( value: ["accountId": "1", "phoneNumber": phoneNumber, "authorised": false, "smsAuthenticationCode": "1234", "deviceToken": "deviceToken", "publicKey": "secretKey"])
   }

    func assertAccount(account: Account){
        XCTAssert(account.getAccountId() == "1")
        XCTAssert(account.getPhoneNumber() == expectedPhoneNumber)
        XCTAssert(!account.isAuthorised() )
        XCTAssert(account.getSMSAuthenticationCode() == "1234")
        XCTAssert(account.getDeviceToken() == "deviceToken")
        XCTAssert(account.getPublicKey() == "secretKey")
    }

   func testPerformanceExample() {
       // This is an example of a performance test case.
       self.measure {
           // Put the code you want to measure the time of here.
       }
   }
    
}
